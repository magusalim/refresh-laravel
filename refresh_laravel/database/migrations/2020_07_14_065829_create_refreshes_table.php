<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefreshesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refreshes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 30);
            $table->string('name', 30);
            $table->string('npm_nis', 15);
            $table->string('age', 2);
            $table->string('gender', 15);
            $table->string('phone', 15);
            $table->string('address', 30);
            $table->string('origins', 30);
            $table->string('location', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refreshes');
    }
}
