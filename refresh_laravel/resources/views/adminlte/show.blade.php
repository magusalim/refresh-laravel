@extends('home')

@section('content')

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Name</th>
                    <th>NPM/NIS</th>
                    <th>Age</th>
                    <th>Gender</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Origins</th>
                    <th>Location</th>
                    <th colspan="2"></th>
                </tr>
            </thead>
            <tbody>

                @foreach ($data as $d)
                <tr>
                    <td>{{$d->email}}</td>
                    <td>{{$d->name}}</td>
                    <td>{{$d->npm_nis}}</td>
                    <td>{{$d->age}}</td>
                    <td>{{$d->gender}}</td>
                    <td>{{$d->phone}}</td>
                    <td>{{$d->address}}</td>
                    <td>{{$d->origins}}</td>
                    <td>{{$d->location}}</td>
                    <td><button class="btn btn-warning">Edit</button></td>
                    <td><button class="btn btn-danger">Delete</button></td>
                </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>

@endsection